﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Net;

namespace stepmedia.unittest
{
    [TestClass]
    public class UnitTestApi
    {
        [TestClass]
        public class UnitTest
        {
            #region Variable
            const bool IsLocalTest = true;
            const string BaseUrl = IsLocalTest ? "http://localhost:29797" : "http://localhost:29797/";

            private TestContext Instance;

            /// <summary>
            /// Gets or sets the test context which provides
            /// information about and functionality for the current test run.
            /// </summary>
            public TestContext TestContext
            {
                get
                {
                    return Instance;
                }
                set
                {
                    Instance = value;
                }
            }
            #endregion

            #region Test Method
            /// <summary>
            /// test trang chủ,...
            /// </summary>
            /// <param name="url"></param>
            [TestMethod]
            [DataRow(BaseUrl + "/fdadadad/Error")]
            [DataRow(BaseUrl + "/Restaurant/SearchRestaurant?PageIndex=0&PageSize=30&OrderType=0")]
            [DataRow(BaseUrl + "/Customer/SearchCustomer?PageIndex=0&PageSize=30&OrderType=0")]
            [DataRow(BaseUrl + "/Table/SearchTable?PageIndex=0&PageSize=30&OrderType=0")]
            public void TestPageStatus(string url)
            {
                var startTime = DateTime.Now;
                if (IsLocalTest)
                    this.Instance.WriteLine("Test Url: " + url);
                Assert.AreEqual(true, GetPage(url));
            }

            public bool GetPage(string url)
            {
                try
                {
                    var currDate = DateTime.Now;
                    var client = new RestClient(url);
                    var request = new RestRequest()
                    {
                        Method = Method.Get
                    };
                    request.AddHeader("User-Agent", "PostmanRuntime");
                    //request.User = "PostmanRuntime";
                    var response = client.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                    {
                        return true;
                    }
                    NotifyHelper.NotifyLine($"GetPage {Environment.MachineName}", response.ErrorMessage, $"{url} - status {response.StatusCode} - {DateTime.Now.Subtract(currDate).TotalMilliseconds} ms Failed - {response.Content}");
                }
                catch (WebException ex)
                {
                    NotifyHelper.NotifyLine($"GetPage {Environment.MachineName}", ex.Source, ex.Message);
                    return false;
                }
                catch (Exception ex)
                {
                    NotifyHelper.NotifyLine($"GetPage {Environment.MachineName}", ex.Source, ex.Message);
                    return false;
                }
                return false;
            }
            #endregion
        }
    }
}
