using RestSharp;
using System;

namespace stepmedia.unittest
{
    public static class NotifyHelper
    {
        public static void NotifyLine(string method, string exception, string message = "", string token = "")
        {
            try
            {
                var messagePost = (string.IsNullOrEmpty(method) ? "" : "`Method:` *" + method + "*\n") + (string.IsNullOrEmpty(exception) ? "" : "`Exception:` *" + exception + "*\n") +
                       (string.IsNullOrEmpty(message) ? "" : "`Exception:` *" + message + "*\n");

                var client = new RestClient("https://notify-api.line.me/api/notify");
                var request = new RestRequest()
                {
                    Method = Method.Post,
                    Timeout = 1000
                };
                if (string.IsNullOrEmpty(token))
                {
                    token = "59fBWdsJpp4evTUUho5sgiGlepSlO8eldfFlMuU9rAY";
                }
                request.AddHeader("Authorization", "Bearer " + token);
                request.AlwaysMultipartFormData = true;
                request.AddParameter("message", messagePost);
                var response = client.Execute(request);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
