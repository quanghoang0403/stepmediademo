﻿using stepmedia.model.Request.Restaurant;
using Microsoft.AspNetCore.Mvc;
using stepmedia.service.RestaurantService;

namespace Demo.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(
            IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        [HttpGet()]
        public async Task<IActionResult> SearchRestaurant([FromQuery] RestaurantSearchRequest request)
        {
            var restaurants = await _restaurantService.SearchPaging(request);
            if (restaurants == null || restaurants.Count < 3)
                return BadRequest();
            return Ok(restaurants);
        }


        [HttpPost]
        public async Task<IActionResult> AddRestaurant([FromBody] RestaurantCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var data = await _restaurantService.Create(request);
            if (data == null || data.Id == 0)
                return BadRequest();
            return CreatedAtAction(nameof(AddRestaurant), new { id = data.Id }, data);
        }


    }
}