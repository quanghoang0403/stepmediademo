﻿using stepmedia.service.OrderService;
using stepmedia.model.Request.Order;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(
            IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet()]
        public async Task<IActionResult> SearchOrder([FromQuery] OrderSearchRequest request)
        {
            var orders = await _orderService.SearchPaging(request);
            return Ok(orders);
        }


        [HttpPost]
        public async Task<IActionResult> AddOrder([FromBody] OrderCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var id = await _orderService.Create(request);
            if (id == 0)
                return BadRequest();
            return CreatedAtAction(nameof(AddOrder), new { id = id }, id);
        }


    }
}