﻿using stepmedia.service.CustomerService;
using stepmedia.model.Request.Customer;
using Microsoft.AspNetCore.Mvc;

namespace Demo.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(
            ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet()]
        public async Task<IActionResult> SearchCustomer([FromQuery] CustomerSearchRequest request)
        {
            var customers = await _customerService.SearchPaging(request);
            if (customers == null || customers.Count < 30)
                return BadRequest();
            return Ok(customers);
        }


        [HttpPost]
        public async Task<IActionResult> AddCustomer([FromBody] CustomerCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var data = await _customerService.Create(request);
            if (data == null || data.Id == 0)
                return BadRequest();
            return CreatedAtAction(nameof(AddCustomer), new { id = data.Id }, data);
        }


    }
}