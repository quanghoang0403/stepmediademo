﻿using stepmedia.service.TableService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using stepmedia.model.Request.Table;

namespace Demo.WebApi.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class TableController : ControllerBase
    {
        private readonly ITableService _tableService;

        public TableController(
            ITableService tableService)
        {
            _tableService = tableService;
        }

        [HttpGet()]
        public async Task<IActionResult> SearchTable([FromQuery] TableSearchRequest request)
        {
            var tables = await _tableService.SearchPaging(request);
            if (tables == null || tables.Count < 30)
                return BadRequest();
            return Ok(tables);
        }


        [HttpPost]
        public async Task<IActionResult> AddTable([FromBody] TableCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var id = await _tableService.Create(request);
            if (id == 0)
                return BadRequest();
            return CreatedAtAction(nameof(AddTable), new { id = id }, id);
        }


    }
}