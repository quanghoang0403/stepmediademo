﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.model.Models
{
    public class OrderVm
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int RestaurantId { get; set; }
        public CustomerVm Customer { get; set; }
        public RestaurantVm Restaurant { get; set; }
        public List<OrderDetailVm> OrderDetailList { get; set; }
    }

    public class OrderDetailVm
    {
        public int TableId { get; set; }
        public int OrderId { get; set; }
        public decimal TotalPrice { set; get; }
        public TableVm Table { get; set; }
    }
}
