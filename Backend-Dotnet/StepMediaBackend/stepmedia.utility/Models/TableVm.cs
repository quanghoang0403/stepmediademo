﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.model.Models
{
    public class TableVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public RestaurantVm Restaurant { get; set; }
    }
}
