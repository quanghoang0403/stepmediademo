﻿using System.ComponentModel.DataAnnotations;

namespace stepmedia.model.Request.OrderDetail
{
    public class OrderDetailCreateRequest
    {
        public int TableId { get; set; }
        public decimal TotalPrice { set; get; }
    }
}