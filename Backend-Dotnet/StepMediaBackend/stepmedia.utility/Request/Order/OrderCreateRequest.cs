﻿using stepmedia.model.Request.OrderDetail;
using System.ComponentModel.DataAnnotations;

namespace stepmedia.model.Request.Order
{
    public class OrderCreateRequest
    {
        public int CustomerId { get; set; }
        public int RestaurantId { get; set; }
        public List<OrderDetailCreateRequest> OrderDetailList { get; set; }
    }
}