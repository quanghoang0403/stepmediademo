﻿using System.ComponentModel.DataAnnotations;

namespace stepmedia.model.Request.Customer
{
    public class CustomerCreateRequest
    {
        public string FullName { get; set; }
        public DateTime Dob { get; set; }
        public string Email { get; set; }

    }
}