﻿using System.ComponentModel.DataAnnotations;

namespace stepmedia.model.Request.Restaurant
{
    public class RestaurantCreateRequest
    {
        public string Name { get; set; }

        public string Location { get; set; }
    }
}