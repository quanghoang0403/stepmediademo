﻿using System.ComponentModel.DataAnnotations;

namespace stepmedia.model.Request.Table
{
    public class TableCreateRequest
    {
        public string Name { get; set; }
        public int RestaurantId { get; set; }
    }
}