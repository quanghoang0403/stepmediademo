﻿using stepmedia.data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace stepmedia.data.Configs
{
    public class TableConfig : IEntityTypeConfiguration<Table>
    {
        public void Configure(EntityTypeBuilder<Table> builder)
        {
            builder.ToTable("Tables");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.RestaurantId).IsRequired();
            builder.HasOne(x => x.Restaurant)
                   .WithMany(t => t.TableList)
                   .HasForeignKey(s => s.RestaurantId);
        }
    }
}
