﻿using stepmedia.data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace stepmedia.data.Configs
{
    public class OrderConfig : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");
            builder.HasKey(t => t.Id);
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.Property(x => x.RestaurantId).IsRequired();
            builder.Property(x => x.CustomerId).IsRequired();
        }
    }
}
