﻿using stepmedia.data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace stepmedia.data.Configs
{
    public class OrderDetailConfig : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.ToTable("OrderDetails");
            builder.HasKey(x => new {x.OrderId, x.TableId });
            builder.Property(x => x.TotalPrice).IsRequired();

            builder.HasOne(t => t.Order).WithMany(x => x.OrderDetailList)
                .HasForeignKey(pc=>pc.OrderId);

            builder.HasOne(x => x.Table).WithMany(x => x.OrderDetailList)
              .HasForeignKey(pc => pc.TableId);
        }
    }
}
