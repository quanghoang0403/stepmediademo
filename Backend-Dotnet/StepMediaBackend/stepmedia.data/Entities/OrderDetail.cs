﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.data.Entities
{
    public class OrderDetail
    {
        public int TableId { get; set; }
        public int OrderId { get; set; }
        public decimal TotalPrice { set; get; }
        public Table Table { get; set; }
        public Order Order { get; set; }
    }
}
