﻿using stepmedia.data.EF;
using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Customer;
using stepmedia.model.Request.Table;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.CustomerService
{
    public class CustomerService : ICustomerService
    {
        private readonly DemoDbContext _context;

        public CustomerService(DemoDbContext context)
        {
            _context = context;
        }
        public async Task<Customer> Create(CustomerCreateRequest request)
        {
            var data = new Customer()
            {
                FullName = request.FullName,
                Dob = request.Dob,
                Email = request.Email,
            };
            _context.Customers.Add(data);
            await _context.SaveChangesAsync();
            return data;
        }


        public async Task<List<CustomerVm>> SearchPaging(CustomerSearchRequest request)
        {
            var data = await _context.Customers.OrderBy(x => x.Email)
                .Skip(request.PageIndex * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new CustomerVm()
                {
                    Id = x.Id,
                    FullName = x.FullName,
                    Dob = x.Dob,
                    Email = x.Email,
                }).ToListAsync();

            return data;
        }
    }
}
