﻿using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Customer;
using stepmedia.model.Request.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.CustomerService
{
    public interface ICustomerService
    {
        Task<Customer> Create(CustomerCreateRequest request);

        Task<List<CustomerVm>> SearchPaging(CustomerSearchRequest request);
    }
}
