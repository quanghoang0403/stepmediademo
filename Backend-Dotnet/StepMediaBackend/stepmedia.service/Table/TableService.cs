﻿using stepmedia.data.EF;
using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Table;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.TableService
{
    public class TableService : ITableService
    {
        private readonly DemoDbContext _context;

        public TableService(DemoDbContext context)
        {
            _context = context;
        }
        public async Task<int> Create(TableCreateRequest request)
        {
            var restaurant = await _context.Restaurants.FindAsync(request.RestaurantId);
            if (restaurant == null || restaurant.Id == 0)
                return 0;
            var data = new Table()
            {
                Name = request.Name,
                RestaurantId = request.RestaurantId,
            };
            _context.Tables.Add(data);
            await _context.SaveChangesAsync();
            return data.Id;
        }

        public async Task<List<TableVm>> SearchPaging(TableSearchRequest request)
        {
            var data = await _context.Tables.OrderBy(x => x.Name)
                .Skip(request.PageIndex * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new TableVm()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Restaurant = new RestaurantVm() { 
                        Name = x.Restaurant.Name,
                        Id = x.Restaurant.Id,
                        Location = x.Restaurant.Location
                    }
                }).ToListAsync();

            return data;
        }
    }
}
