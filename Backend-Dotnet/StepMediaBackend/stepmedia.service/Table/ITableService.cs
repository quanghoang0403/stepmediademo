﻿using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.TableService
{
    public interface ITableService
    {
        Task<int> Create(TableCreateRequest request);
        Task<List<TableVm>> SearchPaging(TableSearchRequest request);
    }
}
