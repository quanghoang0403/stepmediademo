﻿using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.OrderService
{
    public interface IOrderService
    {
        Task<int> Create(OrderCreateRequest request);

        Task<List<OrderVm>> SearchPaging(OrderSearchRequest request);

    }
}
