﻿using stepmedia.data.EF;
using stepmedia.data.Entities;
using stepmedia.service.OrderService;
using stepmedia.model.Models;
using stepmedia.model.Request.Order;
using stepmedia.model.Request.OrderDetail;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.OrderService
{
    public class OrderService : IOrderService
    {
        private readonly DemoDbContext _context;

        public OrderService(DemoDbContext context)
        {
            _context = context;
        }
        public async Task<int> Create(OrderCreateRequest request)
        {
            var data = new Order()
            {
                CustomerId = request.CustomerId,
                RestaurantId = request.RestaurantId,
            };

            var customer = await _context.Customers.FindAsync(request.CustomerId);
            if (customer == null || customer.Id == 0)
                return 0;

            var restaurant = await _context.Restaurants.FindAsync(request.RestaurantId);
            if (restaurant == null || restaurant.Id == 0)
                return 0;

            _context.Orders.Add(data);
            await _context.SaveChangesAsync();

            foreach (var orderDetailRequest in request.OrderDetailList)
            {
                if (orderDetailRequest != null)
                {
                    var table = await _context.Tables.FindAsync(orderDetailRequest.TableId);
                    if (table == null || table.Id == 0)
                        continue;
                    var orderDetail = new OrderDetail()
                    {
                        OrderId = data.Id,
                        TableId = orderDetailRequest.TableId,
                        TotalPrice = orderDetailRequest.TotalPrice,
                    };
                    _context.OrderDetails.Add(orderDetail);
                }
            }
            await _context.SaveChangesAsync();
            return data.Id;
        }
        public async Task<List<OrderVm>> SearchPaging(OrderSearchRequest request)
        {
            var query = from o in _context.Orders
                        join s in _context.Restaurants on o.RestaurantId equals s.Id
                        join c in _context.Customers on o.CustomerId equals c.Id
                        select new { o, s, c };
            var data = await query.OrderBy(x => x.c.Email).ThenByDescending(x => x.s.Location)
                .Skip(request.PageIndex * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new OrderVm()
                {
                    Id = x.o.Id,
                    CustomerId = x.o.CustomerId,
                    RestaurantId = x.o.RestaurantId,
                    Customer = new CustomerVm()
                    {
                        Id = x.c.Id,
                        FullName = x.c.FullName,
                        Dob = x.c.Dob,
                        Email = x.c.Email
                    },
                    Restaurant = new RestaurantVm()
                    {
                        Id = x.s.Id,
                        Name = x.s.Name,
                        Location = x.s.Location
                    },
                    OrderDetailList = x.o.OrderDetailList.OrderBy(order => order.Table.Name).Select(order => new OrderDetailVm()
                    {
                        OrderId = order.OrderId,
                        TableId = order.TableId,
                        TotalPrice = order.TotalPrice,
                        Table = new TableVm()
                        {
                            Id = order.Table.Id,
                            Name = order.Table.Name,
                        }
                    }).ToList()
                }).ToListAsync();

            return data;
        }
    }
}
