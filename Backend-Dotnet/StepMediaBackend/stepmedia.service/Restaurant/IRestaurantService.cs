﻿using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Table;
using stepmedia.model.Request.Restaurant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.RestaurantService
{
    public interface IRestaurantService
    {
        Task<Restaurant> Create(RestaurantCreateRequest request);

        Task<List<RestaurantVm>> SearchPaging(RestaurantSearchRequest request);

    }
}
