﻿using stepmedia.data.EF;
using stepmedia.data.Entities;
using stepmedia.model.Models;
using stepmedia.model.Request.Restaurant;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stepmedia.service.RestaurantService
{
    public class RestaurantService : IRestaurantService
    {
        private readonly DemoDbContext _context;

        public RestaurantService(DemoDbContext context)
        {
            _context = context;
        }
        public async Task<Restaurant> Create(RestaurantCreateRequest request)
        {
            var data = new Restaurant()
            {
                Name = request.Name,
                Location = request.Location,
            };
            _context.Restaurants.Add(data);
            await _context.SaveChangesAsync();
            return data;
        }

        public async Task<List<RestaurantVm>> SearchPaging(RestaurantSearchRequest request)
        {
            var data = await _context.Restaurants.OrderByDescending(x => x.Location)
                .Skip(request.PageIndex * request.PageSize)
                .Take(request.PageSize)
                .Select(x => new RestaurantVm()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Location = x.Location,
                    TableList = x.TableList.Select(tb => new TableVm()
                    {
                        Id = tb.Id,
                        Name = tb.Name,
                    }).ToList()
                }).ToListAsync();

            return data;
        }
    }
}
