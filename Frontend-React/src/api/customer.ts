import { AddCustomerRequest, BaseResult, SearchRequest, SearchResult } from 'models/common';
import axiosClient from './axiosClient';
import { Customer } from 'models/customer';

const path = '/Customer';
const customerApi = {
  addCustomer(params: AddCustomerRequest): Promise<Customer> {
    return axiosClient.post(path + '/AddCustomer', params);
  },
  searchCustomer(params: SearchRequest): Promise<Customer[]> {
    return axiosClient.get(path + '/SearchCustomer', {
      params,
    });
  },
};

export default customerApi;
