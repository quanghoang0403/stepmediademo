import { AddOrderRequest, BaseResult, SearchRequest, SearchResult } from 'models/common';
import { Order } from 'models/order';
import axiosClient from './axiosClient';

const path = '/Order';
const orderApi = {
  addOrder(params: AddOrderRequest): Promise<Order> {
    return axiosClient.post(path + '/AddOrder', params);
  },
  searchOrder(params: SearchRequest): Promise<Order[]> {
    return axiosClient.get(path + '/SearchOrder', {
      params,
    });
  },
};

export default orderApi;
