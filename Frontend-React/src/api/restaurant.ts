import { AddRestaurantRequest, BaseResult, SearchRequest, SearchResult } from 'models/common';
import { Restaurant } from 'models/restaurant';
import axiosClient from './axiosClient';

const path = '/Restaurant';
const restaurantApi = {
  addRestaurant(params: AddRestaurantRequest): Promise<Restaurant> {
    return axiosClient.post(path + '/AddRestaurant', params);
  },
  searchRestaurant(params: SearchRequest): Promise<Restaurant[]> {
    return axiosClient.get(path + '/SearchRestaurant', {
      params,
    });
  },
};

export default restaurantApi;
