import { AddTableRequest, BaseResult, SearchRequest, SearchResult } from 'models/common';
import { Table } from 'models/table';
import axiosClient from './axiosClient';

const path = '/Table';
const tableApi = {
  addTable(params: AddTableRequest): Promise<Table> {
    return axiosClient.post(path + '/AddTable', params);
  },
  searchTable(params: SearchRequest): Promise<Table[]> {
    return axiosClient.get(path + '/SearchTable', {
      params,
    });
  },
};

export default tableApi;
