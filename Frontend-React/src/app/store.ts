import { configureStore, ThunkAction, Action, combineReducers, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './rootSaga';
import commonReducer from 'features/common/commonSlice';
import restaurantReducer from 'features/restaurant/restaurantSlice';
import tableReducer from 'features/table/tableSlice';
import customerReducer from 'features/customer/customerSlice';
import orderReducer from 'features/order/orderSlice';

const rootReducer = combineReducers({
  common: commonReducer,
  restaurant: restaurantReducer,
  table: tableReducer,
  customer: customerReducer,
  order: orderReducer,
});

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: rootReducer,
  devTools: true,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
