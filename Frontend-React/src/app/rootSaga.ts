import customerSaga from 'features/customer/customerSaga';
import orderSaga from 'features/order/orderSaga';
import tableSaga from 'features/table/tableSaga';
import restaurantSaga from 'features/restaurant/restaurantSaga';
import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([restaurantSaga(), tableSaga(), customerSaga(), orderSaga()]);
}
