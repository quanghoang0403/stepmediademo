import { Route, Router, Routes } from 'react-router-dom';
import WebHeader from './WebHeader';
import { NotFound } from 'components/Common/NotFound';
import CustomerPage from 'features/customer/pages/Customer';
import TablePage from 'features/table/pages/Table';
import RestaurantPage from 'features/restaurant/pages/Restaurant';
import OrderPage from 'features/order/pages/Order';
import { useEffect } from 'react';
import { restaurantActions } from 'features/restaurant/restaurantSlice';
import Loading from 'features/common/Loading';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { requestGetAll } from 'models/common';

export interface WebLayoutProps {}

export function WebLayout(props: WebLayoutProps) {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(restaurantActions.loadListRestaurant(requestGetAll));
  }, []);
  return (
    <div>
      {/* <Loading /> */}
      <WebHeader />
      <Routes>
        <Route path="/" element={<OrderPage />} />
        <Route path="/table" element={<TablePage />} />
        <Route path="/customer" element={<CustomerPage />} />
        <Route path="/restaurant" element={<RestaurantPage />} />
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </div>
  );
}
