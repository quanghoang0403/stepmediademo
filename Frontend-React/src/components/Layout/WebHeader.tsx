import * as React from 'react';
import { Link } from 'react-router-dom';

export default function WebHeader() {
  return (
    <div
      style={{
        display: 'flex',
        backgroundColor: 'var(--background-color)',
        height: '60px',
        padding: '8px 5vw',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <div>
        <Link style={{ height: '100%', padding: '5px 10px 0' }} to="/">
          Order
        </Link>
        <Link style={{ height: '100%', padding: '5px 10px 0' }} to="/customer">
          Customer
        </Link>
        <Link style={{ height: '100%', padding: '5px 10px 0' }} to="/restaurant">
          Restaurant
        </Link>
        <Link style={{ height: '100%', padding: '5px 10px 0' }} to="/table">
          Table
        </Link>
      </div>
    </div>
  );
}
