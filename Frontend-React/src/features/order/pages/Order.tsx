import { AddOrderRequest, SearchRequest, requestGetAll } from 'models/common';
import { useEffect, useState } from 'react';
import { convertStringToNumber, formatCurrency } from 'utils/convert';
import { selectListRestaurant, restaurantActions } from 'features/restaurant/restaurantSlice';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { orderActions, selectListOrder } from '../orderSlice';
import * as Yup from 'yup';
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from 'formik';
import { Restaurant } from 'models/restaurant';
import { Table } from 'models/table';
import { customerActions, selectListCustomer } from 'features/customer/customerSlice';
import { commonActions } from 'features/common/commonSlice';

export interface IOrderPageProps {}

export default function OrderPage(props: IOrderPageProps) {
  const dispatch = useAppDispatch();

  const listRestaurant = useAppSelector(selectListRestaurant);
  const listCustomer = useAppSelector(selectListCustomer);
  const listOrder = useAppSelector(selectListOrder);

  const [restaurantId, setRestaurantId] = useState<string>('');
  const [customerId, setCustomerId] = useState<string>('');
  const [tableIds, setTableIds] = useState<string[]>([]);
  const [listTableByRestaurantId, setListTableByRestaurantId] = useState<Table[]>(listRestaurant[0]?.tableList);
  const [listRestaurantHaveTable, setListRestaurantHaveTable] = useState<Restaurant[]>([]);

  useEffect(() => {
    dispatch(commonActions.offPopup());
    if (listRestaurant == null || listRestaurant.length === 0) {
      dispatch(restaurantActions.loadListRestaurant(requestGetAll));
    }
    if (listCustomer == null || listCustomer.length === 0) {
      dispatch(customerActions.loadListCustomer(requestGetAll));
    }
    if (listOrder == null || listOrder.length === 0) {
      dispatch(orderActions.loadListOrder(requestGetAll));
    }
  }, []);

  useEffect(() => {
    setListRestaurantHaveTable(listRestaurant.filter((x) => x.tableList != null && x.tableList.length > 0));
    setRestaurantId(listRestaurantHaveTable[0]?.id.toString());
    resetListTableByRestaurantId();
  }, [listRestaurant]);

  useEffect(() => {
    setCustomerId(listCustomer[0]?.id.toString());
  }, [listCustomer]);

  useEffect(() => {
    resetListTableByRestaurantId();
  }, [restaurantId]);

  const resetListTableByRestaurantId = () => {
    var resMatch = listRestaurantHaveTable.find((res) => res.id.toString() === restaurantId);
    if (resMatch != null) {
      setListTableByRestaurantId(resMatch.tableList);
    } else console.log('Error from listRestaurant');
  };

  const initialValues: AddOrderRequest = {
    customerId: 0,
    restaurantId: 0,
    orderDetailList: [],
  };

  const handleSubmit = async (values: AddOrderRequest, formikHelpers: FormikHelpers<AddOrderRequest>) => {
    const { resetForm, setSubmitting } = formikHelpers;
    try {
      console.log(values);
      console.log(tableIds);
      const updatedValues: AddOrderRequest = {
        ...values,
        restaurantId: convertStringToNumber(restaurantId),
        customerId: convertStringToNumber(customerId),
        orderDetailList: values.orderDetailList.map((orderDetail, index) => ({
          ...orderDetail,
          tableId: convertStringToNumber(tableIds[index]),
        })),
      };
      console.log(updatedValues);
      dispatch(orderActions.addOrder(updatedValues));
      resetForm();
      setSubmitting(false);
    } catch (error) {
      console.error('API error:', error);
      setSubmitting(false);
    }
  };

  return (
    <div>
      <Formik initialValues={initialValues} onSubmit={handleSubmit}>
        {({ isSubmitting, values, setFieldValue }) => (
          <Form style={{ display: 'flex', flexDirection: 'column', alignItems: 'flexStart', padding: '10px' }}>
            {/* Existing form fields */}
            <div style={{ padding: '10px' }}>
              <label htmlFor="customerId" style={{ marginRight: '10px', width: '100px', display: 'inline-block' }}>
                Customer
              </label>
              <select id="customerId" value={customerId} onChange={(e) => setCustomerId(e.target.value)}>
                {listCustomer.map((cus) => (
                  <option key={cus.id} value={cus.id}>
                    {cus.fullName}
                  </option>
                ))}
              </select>
            </div>
            <div style={{ padding: '10px' }}>
              <label htmlFor="restaurantId" style={{ marginRight: '10px', width: '100px', display: 'inline-block' }}>
                Restaurant
              </label>
              <select id="restaurantId" value={restaurantId} onChange={(e) => setRestaurantId(e.target.value)}>
                {listRestaurantHaveTable.map((res) => (
                  <option key={res.id} value={res.id}>
                    {res.name}
                  </option>
                ))}
              </select>
            </div>
            {/* OrderDetail input blocks */}
            {values.orderDetailList.map((orderDetail, index) => (
              <div key={index} style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ padding: '10px' }}>
                  <label htmlFor={'tableId' + index} style={{ marginRight: '10px', width: '100px', display: 'inline-block' }}>
                    Table
                  </label>
                  <select
                    id={'tableId' + index}
                    value={tableIds[index] || listTableByRestaurantId[0]?.id.toString()}
                    onChange={(e) => {
                      const updatedTableIds = [...tableIds];
                      updatedTableIds[index] = e.target.value;
                      setTableIds(updatedTableIds);
                    }}
                  >
                    {listTableByRestaurantId.map((table) => (
                      <option key={table.id} value={table.id}>
                        {table.name}
                      </option>
                    ))}
                  </select>
                </div>
                <div style={{ padding: '10px' }}>
                  <label htmlFor={`orderDetailList[${index}].totalPrice`} style={{ marginRight: '10px' }}>
                    Total Price
                  </label>
                  <Field type="number" id={`orderDetailList[${index}].totalPrice`} name={`orderDetailList[${index}].totalPrice`} />
                  <ErrorMessage name={`orderDetailList[${index}].totalPrice`} component="div" className="error-message" />
                </div>
              </div>
            ))}

            {/* Button to add more OrderDetail blocks */}
            <button
              type="button"
              style={{ margin: '10px' }}
              onClick={() => {
                const newOrderDetail = { tableId: listTableByRestaurantId[0]?.id, totalPrice: 0 };
                console.log(listRestaurantHaveTable);
                setFieldValue('orderDetailList', [...values.orderDetailList, newOrderDetail]);
              }}
            >
              Add OrderDetail
            </button>

            {/* Submit button */}
            <button style={{ margin: '10px', backgroundColor: 'orange' }} type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </Form>
        )}
      </Formik>
      <h3 style={{ padding: '0 20px' }}>List orrder order by Customer.Email, then order by descending Restaurant.Location, then order by Table.Name </h3>
      <div>
        {listOrder.map((order) => (
          <div key={order.id} style={{ display: 'flex', flexDirection: 'column', padding: '10px', textDecoration: 'none', color: 'black', border: '1px solid gray' }}>
            <p style={{ fontSize: '20px', fontWeight: '700', margin: '0px 10px' }}>#{order.id}</p>
            <div style={{ display: 'flex', flexDirection: 'row', margin: '0 10px' }}>
              <p style={{ marginRight: '50px' }}>Name: {order.customer.fullName}</p>
              <p style={{ marginRight: '50px' }}>Email: {order.customer.email}</p>
              <p style={{ marginRight: '50px' }}>
                Restaurant: {order.restaurant.name} - {order.restaurant.location}
              </p>
            </div>

            <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', flexWrap: 'wrap' }}>
              {order.orderDetailList.map((orderDetail) => (
                <div key={orderDetail.table.id} style={{ flexBasis: '10%', padding: '10px', border: '1px solid gray', textDecoration: 'none', color: 'black' }}>
                  <h3>{orderDetail.table.name}</h3>
                  <p>Price: {formatCurrency(orderDetail.totalPrice)}</p>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
