import orderApi from 'api/order';
import { commonActions } from 'features/common/commonSlice';
import { AddOrderRequest, BaseResult, SearchRequest, requestGetAll } from 'models/common';
import { Order } from 'models/order';
import { take, fork, call, takeLatest, all, put, takeEvery } from 'redux-saga/effects';
import { orderActions } from '../order/orderSlice';
import { PayloadAction } from '@reduxjs/toolkit';

// Worker Saga
function* onLoadListOrderFlow({ payload }: PayloadAction<SearchRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Order[] = yield call(orderApi.searchOrder, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(orderActions.loadListOrderSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (e: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.log(e.message);
  }
}

function* addOrderFlow({ payload }: PayloadAction<AddOrderRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Order = yield call(orderApi.addOrder, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      // yield put(orderActions.addOrderSuccess(response));
      yield put(orderActions.loadListOrder(requestGetAll));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (error: any) {
    yield put(commonActions.showPopup('Lỗi không đủ dữ liệu'));
    console.error(error.message);
  }
}

// Watcher saga - watch for every action of type LOAD_IMAGES, and trigger onLoadImages saga
function* watchLoadOrderListFlow() {
  yield takeEvery(orderActions.loadListOrder.type, onLoadListOrderFlow);
}

function* watchAddOrderFlow() {
  yield takeEvery(orderActions.addOrder.type, addOrderFlow);
}

// Run all the watcher sagas
export default function* OrderSaga() {
  yield all([watchLoadOrderListFlow(), watchAddOrderFlow()]);
}
