import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';
import { AddOrderRequest, SearchRequest } from 'models/common';
import { Order } from 'models/order';

export interface OrderState {
  listOrder: Order[];
}

const initialState: OrderState = {
  listOrder: [],
};

const orderSlice = createSlice({
  name: 'order',
  initialState,
  reducers: {
    loadListOrder: (state, action: PayloadAction<SearchRequest>) => {},
    loadListOrderSuccess: (state, action: PayloadAction<Order[]>) => {
      state.listOrder = action.payload;
    },
    addOrder: (state, action: PayloadAction<AddOrderRequest>) => {},
    addOrderSuccess: (state, action: PayloadAction<Order>) => {
      state.listOrder = [...state.listOrder, action.payload];
    },
  },
});

// Actions
export const orderActions = orderSlice.actions;

// Selectors
export const selectListOrder = (state: RootState) => state.order.listOrder;

// Reducers
const orderReducer = orderSlice.reducer;
export default orderReducer;
