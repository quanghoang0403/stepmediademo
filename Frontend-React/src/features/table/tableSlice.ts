import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';
import { AddTableRequest, SearchRequest } from 'models/common';
import { Table } from 'models/table';

export interface TableState {
  listTable: Table[];
}

const initialState: TableState = {
  listTable: [],
};

const tableSlice = createSlice({
  name: 'table',
  initialState,
  reducers: {
    loadListTable: (state, action: PayloadAction<SearchRequest>) => {},
    loadListTableSuccess: (state, action: PayloadAction<Table[]>) => {
      state.listTable = action.payload;
    },
    addTable: (state, action: PayloadAction<AddTableRequest>) => {},
    addTableSuccess: (state, action: PayloadAction<Table>) => {
      state.listTable = [...state.listTable, action.payload];
    },
  },
});

// Actions
export const tableActions = tableSlice.actions;

// Selectors
export const selectListTable = (state: RootState) => state.table.listTable;

// Reducers
const tableReducer = tableSlice.reducer;
export default tableReducer;
