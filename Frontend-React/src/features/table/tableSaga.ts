import tableApi from 'api/table';
import { commonActions } from 'features/common/commonSlice';
import { AddTableRequest, BaseResult, SearchRequest, requestGetAll } from 'models/common';
import { Table } from 'models/table';
import { take, fork, call, takeLatest, all, put, takeEvery } from 'redux-saga/effects';
import { tableActions } from '../table/tableSlice';
import { PayloadAction } from '@reduxjs/toolkit';
import { restaurantActions } from 'features/restaurant/restaurantSlice';

// Worker Saga
function* onLoadListTableFlow({ payload }: PayloadAction<SearchRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Table[] = yield call(tableApi.searchTable, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(tableActions.loadListTableSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (e: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.log(e.message);
  }
}

function* addTableFlow({ payload }: PayloadAction<AddTableRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Table = yield call(tableApi.addTable, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      //yield put(tableActions.addTableSuccess(response));
      yield put(tableActions.loadListTable(requestGetAll));
      yield put(restaurantActions.loadListRestaurant(requestGetAll));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (error: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.error(error.message);
  }
}

// Watcher saga - watch for every action of type LOAD_IMAGES, and trigger onLoadImages saga
function* watchLoadTableListFlow() {
  yield takeEvery(tableActions.loadListTable.type, onLoadListTableFlow);
}

function* watchAddTableFlow() {
  yield takeEvery(tableActions.addTable.type, addTableFlow);
}

// Run all the watcher sagas
export default function* TableSaga() {
  yield all([watchLoadTableListFlow(), watchAddTableFlow()]);
}
