import { AddTableRequest, SearchRequest, requestGetAll } from 'models/common';
import { Table } from 'models/table';
import * as React from 'react';
import { useEffect, useState } from 'react';
import tableApi from 'api/table';
import { convertStringToNumber, formatCurrency } from 'utils/convert';
import { tableActions, selectListTable } from '../tableSlice';
import { ErrorMessage, Field, FieldProps, Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { restaurantActions, selectListRestaurant } from 'features/restaurant/restaurantSlice';
import Select from 'react-select';
import { commonActions, selectIsShowPopup } from 'features/common/commonSlice';

export interface ITablePageProps {}

export default function TablePage(props: ITablePageProps) {
  const dispatch = useAppDispatch();
  const [restaurantId, setRestaurantId] = useState<string>('');

  const listRestaurant = useAppSelector(selectListRestaurant);
  const listTable = useAppSelector(selectListTable);
  const isError = useAppSelector(selectIsShowPopup);
  useEffect(() => {
    dispatch(commonActions.offPopup());
    if (listRestaurant == null || listRestaurant.length == 0) {
      dispatch(restaurantActions.loadListRestaurant(requestGetAll));
    }
    if (listTable == null || listTable.length == 0) {
      dispatch(tableActions.loadListTable(requestGetAll));
    }
  }, []);

  // const listRestaurant = useAppSelector(selectListRestaurant);

  const initialValues: AddTableRequest = {
    name: '',
    restaurantId: 0,
  };

  const validationSchema = Yup.object({
    name: Yup.string().required('Name is required'),
    restaurantId: Yup.number().required('Restaurant is required'),
  });

  const handleSubmit = async (values: AddTableRequest, formikHelpers: FormikHelpers<AddTableRequest>) => {
    const { resetForm, setSubmitting } = formikHelpers;

    try {
      values.restaurantId = convertStringToNumber(restaurantId);
      dispatch(tableActions.addTable(values));
      resetForm();
      setSubmitting(false);
    } catch (error) {
      console.error('API error:', error);
      setSubmitting(false);
    }
  };

  return (
    <div>
      <h3 style={{ padding: '0 20px' }}>Add table</h3>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
        {({ isSubmitting }) => (
          <Form style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '10px' }}>
            <div style={{ padding: '10px' }}>
              <label htmlFor="name" style={{ marginRight: '10px' }}>
                Name
              </label>
              <Field type="text" id="name" name="name" />
              <ErrorMessage name="name" component="div" />
            </div>
            <div style={{ display: 'flex', padding: '20px', margin: '20px' }}>
              <label htmlFor="restaurantId" style={{ marginRight: '10px' }}>
                Restaurant
              </label>
              <select id="restaurantId" value={restaurantId} onChange={(e) => setRestaurantId(e.target.value)}>
                {listRestaurant.map((res) => (
                  <option key={res.id} value={res.id}>
                    {res.name}
                  </option>
                ))}
              </select>
            </div>
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
      <h3 style={{ padding: '0 20px' }}>List Table order by: Table.Name</h3>

      <div>
        {listTable.map((table) => (
          <div
            key={table.id}
            style={{ display: 'flex', flexDirection: 'column', flexBasis: '16.5%', padding: '10px', border: '1px solid gray', textDecoration: 'none', color: 'black' }}
          >
            <p style={{ fontSize: '20px', fontWeight: '700', margin: '0px 10px' }}>{table.name}</p>
            <p style={{ fontSize: '18px', fontWeight: '500', margin: '0px 10px' }}>
              {table.restaurant.name} - {table.restaurant.location}
            </p>
          </div>
        ))}
      </div>
      {isError && (
        <div>
          <h3 style={{ padding: '0 20px', color: 'red' }}>Error: Dont have enough data</h3>
        </div>
      )}
    </div>
  );
}
