import customerApi from 'api/customer';
import { commonActions } from 'features/common/commonSlice';
import { AddCustomerRequest, BaseResult, SearchRequest } from 'models/common';
import { Customer } from 'models/customer';
import { take, fork, call, takeLatest, all, put, takeEvery } from 'redux-saga/effects';
import { customerActions } from '../customer/customerSlice';
import { PayloadAction } from '@reduxjs/toolkit';

// Worker Saga
function* onLoadListCustomerFlow({ payload }: PayloadAction<SearchRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Customer[] = yield call(customerApi.searchCustomer, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(customerActions.loadListCustomerSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (e: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.log(e.message);
  }
}

function* addCustomerFlow({ payload }: PayloadAction<AddCustomerRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Customer = yield call(customerApi.addCustomer, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(customerActions.addCustomerSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (error: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.error(error.message);
  }
}

// Watcher saga - watch for every action of type LOAD_IMAGES, and trigger onLoadImages saga
function* watchLoadCustomerListFlow() {
  yield takeEvery(customerActions.loadListCustomer.type, onLoadListCustomerFlow);
}

function* watchAddCustomerFlow() {
  yield takeEvery(customerActions.addCustomer.type, addCustomerFlow);
}

// Run all the watcher sagas
export default function* CustomerSaga() {
  yield all([watchLoadCustomerListFlow(), watchAddCustomerFlow()]);
}
