import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';
import { AddCustomerRequest, SearchRequest } from 'models/common';
import { Customer } from 'models/customer';

export interface CustomerState {
  listCustomer: Customer[];
}

const initialState: CustomerState = {
  listCustomer: [],
};

const customerSlice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    loadListCustomer: (state, action: PayloadAction<SearchRequest>) => {},
    loadListCustomerSuccess: (state, action: PayloadAction<Customer[]>) => {
      state.listCustomer = action.payload;
    },
    addCustomer: (state, action: PayloadAction<AddCustomerRequest>) => {},
    addCustomerSuccess: (state, action: PayloadAction<Customer>) => {
      state.listCustomer = [...state.listCustomer, action.payload];
    },
  },
});

// Actions
export const customerActions = customerSlice.actions;

// Selectors
export const selectListCustomer = (state: RootState) => state.customer.listCustomer;

// Reducers
const customerReducer = customerSlice.reducer;
export default customerReducer;
