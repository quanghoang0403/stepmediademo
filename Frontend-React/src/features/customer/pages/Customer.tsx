import customerApi from 'api/customer';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { AddCustomerRequest, SearchRequest, requestGetAll } from 'models/common';
import { Customer } from 'models/customer';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { selectListCustomer, customerActions } from '../customerSlice';
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { commonActions, selectIsShowPopup } from 'features/common/commonSlice';

export interface ICustomerPageProps {}

export default function CustomerPage(props: ICustomerPageProps) {
  const dispatch = useAppDispatch();
  const listCustomer = useAppSelector(selectListCustomer);
  const isError = useAppSelector(selectIsShowPopup);
  useEffect(() => {
    dispatch(commonActions.offPopup());
    if (listCustomer == null || listCustomer.length == 0) {
      dispatch(customerActions.loadListCustomer(requestGetAll));
    }
  }, []);

  const initialValues: AddCustomerRequest = {
    fullName: '',
    email: '',
    dob: new Date(),
  };

  const validationSchema = Yup.object({
    fullName: Yup.string().required('Name is required'),
    email: Yup.string().required('Email is required'),
    dob: Yup.date().required('Date of Birth is required'),
  });

  const handleSubmit = async (values: AddCustomerRequest, formikHelpers: FormikHelpers<AddCustomerRequest>) => {
    const { resetForm, setSubmitting } = formikHelpers;
    try {
      dispatch(customerActions.addCustomer(values));
      resetForm();
      setSubmitting(false);
    } catch (error) {
      console.error('API error:', error);
      setSubmitting(false);
    }
  };

  return (
    <div>
      <h3 style={{ padding: '0 20px' }}>Add customer</h3>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
        {({ isSubmitting }) => (
          <Form style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '10px' }}>
            <div style={{ padding: '10px' }}>
              <label htmlFor="fullName" style={{ marginRight: '10px' }}>
                Name
              </label>
              <Field type="text" id="fullName" name="fullName" />
              <ErrorMessage name="fullName" component="div" className="error-message" />
            </div>
            <div style={{ padding: '10px' }}>
              <label htmlFor="email" style={{ marginRight: '10px' }}>
                Email
              </label>
              <Field type="text" id="email" name="email" />
              <ErrorMessage name="email" component="div" className="error-message" />
            </div>
            <div style={{ padding: '10px' }}>
              <label htmlFor="dob" style={{ marginRight: '10px' }}>
                Dob
              </label>
              <Field type="date" id="dob" name="dob" />
              <ErrorMessage name="dob" component="div" className="error-message" />
            </div>
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
      <h3 style={{ padding: '0 20px' }}>List customer order by: Customer.Email</h3>
      <div style={{ display: 'flex', flexDirection: 'row', padding: '10px', flexWrap: 'wrap' }}>
        {listCustomer.map((customer) => (
          <div
            key={customer.id}
            style={{ display: 'flex', flexDirection: 'column', flexBasis: '16.5%', padding: '10px', border: '1px solid gray', textDecoration: 'none', color: 'black' }}
          >
            <p style={{ fontSize: '20px', fontWeight: '700', margin: '0px 10px' }}>{customer.fullName + ' #' + customer.id}</p>
            <p>Email: {customer.email}</p>
            {/* <p>Dob: {customer.dob}</p> */}
          </div>
        ))}
      </div>

      {isError && (
        <div>
          <h3 style={{ padding: '0 20px', color: 'red' }}>Error: Dont have enough data</h3>
        </div>
      )}
    </div>
  );
}
