import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';
import { AddRestaurantRequest, SearchRequest } from 'models/common';
import { Restaurant } from 'models/restaurant';

export interface RestaurantState {
  listRestaurant: Restaurant[];
}

const initialState: RestaurantState = {
  listRestaurant: [],
};

const restaurantSlice = createSlice({
  name: 'restaurant',
  initialState,
  reducers: {
    loadListRestaurant: (state, action: PayloadAction<SearchRequest>) => {},
    loadListRestaurantSuccess: (state, action: PayloadAction<Restaurant[]>) => {
      state.listRestaurant = action.payload;
    },
    addRestaurant: (state, action: PayloadAction<AddRestaurantRequest>) => {},
    addRestaurantSuccess: (state, action: PayloadAction<Restaurant>) => {
      state.listRestaurant = [...state.listRestaurant, action.payload];
    },
  },
});

// Actions
export const restaurantActions = restaurantSlice.actions;

// Selectors
export const selectListRestaurant = (state: RootState) => state.restaurant.listRestaurant;

// Reducers
const restaurantReducer = restaurantSlice.reducer;
export default restaurantReducer;
