import restaurantApi from 'api/restaurant';
import { commonActions } from 'features/common/commonSlice';
import { AddRestaurantRequest, BaseResult, SearchRequest } from 'models/common';
import { Restaurant } from 'models/restaurant';
import { take, fork, call, takeLatest, all, put, takeEvery } from 'redux-saga/effects';
import { restaurantActions } from '../restaurant/restaurantSlice';
import { PayloadAction } from '@reduxjs/toolkit';

// Worker Saga
function* onLoadListRestaurantFlow({ payload }: PayloadAction<SearchRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Restaurant[] = yield call(restaurantApi.searchRestaurant, payload);

    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(restaurantActions.loadListRestaurantSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (e: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.log(e.message);
  }
}

function* addRestaurantFlow({ payload }: PayloadAction<AddRestaurantRequest>) {
  try {
    yield put(commonActions.loading());
    const response: Restaurant = yield call(restaurantApi.addRestaurant, payload);
    if (response != null) {
      yield put(commonActions.offLoading());
      yield put(commonActions.offPopup());
      yield put(restaurantActions.addRestaurantSuccess(response));
    } else {
      yield put(commonActions.showPopup('Không có dữ liệu'));
    }
  } catch (error: any) {
    yield put(commonActions.showPopup('Lỗi hệ thống, vui lòng thử lại sau'));
    console.error(error.message);
  }
}

// Watcher saga - watch for every action of type LOAD_IMAGES, and trigger onLoadImages saga
function* watchLoadRestaurantListFlow() {
  yield takeEvery(restaurantActions.loadListRestaurant.type, onLoadListRestaurantFlow);
}

function* watchAddRestaurantFlow() {
  yield takeEvery(restaurantActions.addRestaurant.type, addRestaurantFlow);
}

// Run all the watcher sagas
export default function* RestaurantSaga() {
  yield all([watchLoadRestaurantListFlow(), watchAddRestaurantFlow()]);
}
