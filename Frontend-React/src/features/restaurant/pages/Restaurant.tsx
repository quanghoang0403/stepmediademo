import tableApi from 'api/table';
import restaurantApi from 'api/restaurant';
import { useAppDispatch, useAppSelector } from 'app/hooks';
import { AddRestaurantRequest, SearchRequest, requestGetAll } from 'models/common';
import { Restaurant } from 'models/restaurant';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { selectListRestaurant, restaurantActions } from '../restaurantSlice';
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { formatCurrency } from 'utils/convert';
import { selectListTable } from 'features/table/tableSlice';
import { commonActions, selectIsShowPopup } from 'features/common/commonSlice';

export interface IRestaurantPageProps {}

export default function RestaurantPage(props: IRestaurantPageProps) {
  const dispatch = useAppDispatch();
  const listRestaurant = useAppSelector(selectListRestaurant);
  const listTable = useAppSelector(selectListTable);
  const isError = useAppSelector(selectIsShowPopup);
  useEffect(() => {
    dispatch(commonActions.offPopup());
  }, []);

  useEffect(() => {
    if (listRestaurant == null || listRestaurant.length == 0) {
      dispatch(restaurantActions.loadListRestaurant(requestGetAll));
    } else {
    }
  }, [listTable]);

  const initialValues: AddRestaurantRequest = {
    name: '',
    location: '',
  };

  const validationSchema = Yup.object({
    name: Yup.string().required('Name is required'),
    location: Yup.string().required('Location is required'),
  });

  const handleSubmit = async (values: AddRestaurantRequest, formikHelpers: FormikHelpers<AddRestaurantRequest>) => {
    const { resetForm, setSubmitting } = formikHelpers;

    try {
      dispatch(restaurantActions.addRestaurant(values));
      resetForm();
      setSubmitting(false);
    } catch (error) {
      console.error('API error:', error);
      setSubmitting(false);
    }
  };

  return (
    <div>
      <h3 style={{ padding: '0 20px' }}>Add restaurant</h3>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
        {({ isSubmitting }) => (
          <Form style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '10px' }}>
            <div style={{ padding: '10px' }}>
              <label htmlFor="name" style={{ marginRight: '10px' }}>
                Name
              </label>
              <Field type="text" id="name" name="name" />
              <ErrorMessage name="name" component="div" className="error-message" />
            </div>
            <div style={{ padding: '10px' }}>
              <label htmlFor="location" style={{ marginRight: '10px' }}>
                Location
              </label>
              <Field type="text" id="location" name="location" />
              <ErrorMessage name="location" component="div" className="error-message" />
            </div>
            <button type="submit" disabled={isSubmitting}>
              Submit
            </button>
          </Form>
        )}
      </Formik>
      <div>
        <h3 style={{ padding: '0 20px' }}>List Restaurant order by descending: Restaurant.Location </h3>
        {listRestaurant.map((restaurant) => (
          <div
            key={restaurant.id}
            style={{ display: 'flex', flexDirection: 'column', flexBasis: '16.5%', padding: '40px', border: '1px solid gray', textDecoration: 'none', color: 'black' }}
          >
            <p style={{ fontSize: '20px', fontWeight: '700', margin: '0px' }}>{restaurant.name}</p>
            <p>Location: {restaurant.location}</p>

            <div style={{ display: 'flex', flexDirection: 'row', marginBottom: '20px', flexWrap: 'wrap' }}>
              {restaurant.tableList?.map((table) => (
                <div key={table.id} style={{ flexBasis: '10%', padding: '10px', border: '1px solid gray', textDecoration: 'none', color: 'black' }}>
                  <p style={{ fontSize: '20px', fontWeight: '700', margin: '0px 10px' }}>#{table.name}</p>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
      {isError && (
        <div>
          <h3 style={{ padding: '0 20px', color: 'red' }}>Error: Dont have enough data</h3>
        </div>
      )}
    </div>
  );
}
