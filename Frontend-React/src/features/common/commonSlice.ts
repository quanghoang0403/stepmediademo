import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'app/store';

export interface CommonState {
  isLoading: boolean;
  isShowPopup: boolean;
  popupMessage: string;
}

const initialState: CommonState = {
  isLoading: false,
  isShowPopup: false,
  popupMessage: 'Loading...',
};

const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    loading: (state) => {
      state.isLoading = true;
      state.isShowPopup = false;
    },
    offLoading: (state) => {
      state.isLoading = false;
    },
    showPopup: (state, payload: PayloadAction<string>) => {
      state.isShowPopup = true;
      state.popupMessage = payload.payload;
      state.isLoading = false;
    },
    offPopup: (state) => {
      state.isShowPopup = false;
    },
  },
});

// Actions
export const commonActions = commonSlice.actions;

// Selectors
export const selectIsLoading = (state: RootState) => state.common.isLoading;
export const selectIsShowPopup = (state: RootState) => state.common.isShowPopup;
export const selectPopupMessage = (state: RootState) => state.common.popupMessage;

// Reducers
const commonReducer = commonSlice.reducer;
export default commonReducer;
