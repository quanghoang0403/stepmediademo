import * as React from 'react';
import { useAppSelector } from 'app/hooks';
import { selectIsLoading, selectIsShowPopup, selectPopupMessage } from './commonSlice';

export default function Loading() {
  const isLoading = useAppSelector(selectIsLoading);
  const isShowPopup = useAppSelector(selectIsShowPopup);
  const popupMessage = useAppSelector(selectPopupMessage);

  return (
    <div style={{ position: 'fixed', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'rgba(0,0,0,.8)', zIndex: 10 }}>
      <div style={{ position: 'fixed', top: '50%', left: 0, right: 0, bottom: 0, backgroundColor: '#fff', zIndex: 11 }}>
        <h1>Info</h1>
        {isLoading && <p>Loading...</p>}
        {isShowPopup && <p>{popupMessage}</p>}
        {/* Rest of your component code */}
      </div>
    </div>
  );
}
