import { Restaurant } from './restaurant';

export interface Table {
  id: number;
  name: string;
  restaurant: Restaurant;
}
