export interface SearchResult<T> {
  data: T[];
  total: number;
  left: number;
  pageIndex: number;
  pageSize: number;
}

export interface BaseResult<T> {
  data: T;
  code: number;
  message: string;
  errormessage: string;
  serverName: string;
}

export interface SearchRequest {
  pageIndex: number;
  pageSize: number;
  orderId: number;
}

export interface AddTableRequest {
  name: string;
  restaurantId: number;
}

export interface AddCustomerRequest {
  fullName: string;
  dob: Date;
  email: string;
}

export interface AddRestaurantRequest {
  name: string;
  location: string;
}

export interface AddOrderRequest {
  customerId: number;
  restaurantId: number;
  orderDetailList: AddOrderDetailRequest[];
}

export interface AddOrderDetailRequest {
  tableId: number;
  totalPrice: number;
}

export const requestGetAll: SearchRequest = {
  pageIndex: 0,
  pageSize: 100,
  orderId: 0,
};
