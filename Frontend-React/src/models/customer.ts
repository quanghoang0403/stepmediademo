export interface Customer {
  id: number;
  fullName: string;
  dob: Date;
  email: string;
}
