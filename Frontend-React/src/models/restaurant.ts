import { Table } from './table';

export interface Restaurant {
  id: number;
  name: string;
  location: string;
  tableList: Table[];
}
