import { Customer } from './customer';
import { Table } from './table';
import { Restaurant } from './restaurant';

export interface Order {
  id: number;
  restaurant: Restaurant;
  customer: Customer;
  orderDetailList: OrderDetail[];
}

export interface OrderDetail {
  tableId: number;
  table: Table;
  totalPrice: number;
}
