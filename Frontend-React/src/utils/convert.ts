export function formatCurrency(number: number) {
  const formattedNumber = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(number);
  return formattedNumber.replace(/\D00(?=\D*$)/, ''); // Remove trailing '.00' from the formatted string
}

export function convertStringToNumber(str: string) {
  const numeric: number = parseInt(str, 10);
  return numeric;
}
