import { Route, Routes } from 'react-router-dom';
import { WebLayout } from 'components/Layout/WebLayout';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './App.css';
import React from 'react';

function App() {
  return (
    <div>
      {/* <React.StrictMode> */}
      <WebLayout />
      {/* </React.StrictMode> */}
    </div>
  );
}

export default App;
