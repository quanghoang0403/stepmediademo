
1. LinkGitlab: https://gitlab.com/quanghoang0403/stepmediademo

2. LinkSourceCode: https://drive.google.com/file/d/1n3GsBozMoFqzqGoitP4pD7hamureSZ9V/view?usp=sharing

3. LinkDemo: https://www.youtube.com/watch?v=pShL43S5VSY

Techstack:
- Frontend: ReactJS (Typescript, Redux, Redux-saga, Redux-toolkit, Formik)
- Backend: .Net Core (Entity Framework, .Net Web Api, Unit Test)
- Database: SQL Server
